Sample project for OpenClassroom's fundamentals of Java
# Project_DA_Java_EN_Come_to_the_Rescue_of_a_Java_Application

- Symptoms.txt - This should be read into the program.
- No proper treatment of exceptions
- Not closing resources
- Overlapping variables
- The approach in the code is not going to work long term.
- Rewrite it so that it will be reusable no matter how many symptoms we need to track.


- We use JavaDoc as a comment header to each function, so add those.
- He initializes i with 0, then comments as being initialized with 0. Remove those you feel aren’t needed.
- Your code should also follow good object-oriented principles. Alex’s code is one long function.
- It needs to be broken up. I started to do that with the SymptomReader stuff
- Make new classes as you see fit, or move functionality into their own methods.

Otherwise, for projects like this, we tend to use the GitFlow workflow here.
- Make sure to experiment using your local repo,
- Create new branches for each successive version of the code.  I’ll check your work on your release branch.
- Clean up your commits and your branches before showing it to me
- Your code should be streamlined and readable. 

