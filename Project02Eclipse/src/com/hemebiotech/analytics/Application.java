package com.hemebiotech.analytics;

import java.io.IOException;

public class Application {
    /**
     *
     * @param args
     * @throws IOException
     */

    public static void main(String[] args) throws IOException {
        AnalyticsCounter analyticsCounter = new AnalyticsCounter();
        analyticsCounter.doAnalytics("Project02Eclipse/symptoms.txt");
    }
}
