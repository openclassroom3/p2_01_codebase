package com.hemebiotech.analytics;

import java.io.IOException;

/**
 * Reads Symptoms from file and creates result.out file with all the symptoms counted
 */
public class AnalyticsCounter {

	/**
	 *
	 * @param filepath
	 * @throws IOException
	 */
	public void doAnalytics(String filepath) throws IOException {

		ISymptomReader symptomReader = new ReadSymptomDataFromFile(filepath);
		SymptomMap symptomMap = new SymptomMap();
		symptomMap.readSymptoms(symptomReader);
		symptomMap.writeToFile();
		symptomMap.print();

	}
}
