package com.hemebiotech.analytics;

import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

/**
 * Puts the symptoms into a map, and count their occurrences
 * Sorts the SymptomMap alphabetically
 */
public class SymptomMap {

    Map<String, Integer> symptomsMapCount = new TreeMap<>();


    /**
     * Feeds the symptoms from the interface into the map in alphabetical order
     *
     * @param symptomReader
     */
    public void readSymptoms(ISymptomReader symptomReader) {
        for (String symptom : symptomReader.getSymptoms()) {
            if (symptomsMapCount.containsKey(symptom)) {
                int counter = symptomsMapCount.get(symptom);
                symptomsMapCount.put(symptom, ++counter);
            } else {
                symptomsMapCount.put(symptom, 1);
            }
        }
    }
    /**
     * Writes an output of the map into a text file
     * @throws IOException
     */
    public void writeToFile() throws IOException {
        FileWriter writer = new FileWriter("result.out");
        try {
            for (Map.Entry<String, Integer> mapSymptoms : symptomsMapCount.entrySet()) {
                String key = mapSymptoms.getKey();
                Integer value = mapSymptoms.getValue();
                String result = key + ": " + value + "\n";
                writer.write(result);
                writer.flush();
            }
        } finally {
            writer.close();
        }
    }

    /**
     * prints the map into the console output for debugging
     */
    public void print() {

        for (Map.Entry<String, Integer> mapSymptoms : symptomsMapCount.entrySet()) {
            String key = mapSymptoms.getKey();
            Integer value = mapSymptoms.getValue();
            String result = key + ": " + value + "\n";
            System.out.println(result);

        }
    }
}